package omsu.javaprojects.trinomial;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class TrinomialRooterTest {
    @Test(expected = RuntimeException.class)
    public void testDLowerThanZeroOrAIsZero() {
        QuadTrinomial qt = new QuadTrinomial(0, 1, 1);
        TrinomialRooter tr = new TrinomialRooter(qt);
        tr.getMaxRoot();
    }

    @Test
    public void testDEqualsZero() {
        QuadTrinomial qt = new QuadTrinomial(1, 2, 1);
        TrinomialRooter tr = new TrinomialRooter(qt);
        assertEquals(tr.getMaxRoot(), -1, 10E-9);
    }

    @Test
    public void testDBiggerThanZero() {
        QuadTrinomial qt = new QuadTrinomial(1, 2, -3);
        TrinomialRooter tr = new TrinomialRooter(qt);
        assertEquals(tr.getMaxRoot(), 1, 10E-9);
    }

}
